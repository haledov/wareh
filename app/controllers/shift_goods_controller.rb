class ShiftGoodsController < ApplicationController
  before_action :set_shift_good, only: [:show, :edit, :update, :destroy]

  # GET /shift_goods
  # GET /shift_goods.json
  def index
    @shift_goods = ShiftGood.all
  end

  # GET /shift_goods/1
  # GET /shift_goods/1.json
  def show
  end

  # GET /shift_goods/new
  def new
    @shift_good = ShiftGood.new
  end

  # GET /shift_goods/1/edit
  def edit
  end

  # POST /shift_goods
  # POST /shift_goods.json
  def create
    @shift_good = ShiftGood.new(shift_good_params)

    respond_to do |format|
      if @shift_good.save
        format.html { redirect_to @shift_good, notice: 'Shift good was successfully created.' }
        format.json { render :show, status: :created, location: @shift_good }
      else
        format.html { render :new }
        format.json { render json: @shift_good.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /shift_goods/1
  # PATCH/PUT /shift_goods/1.json
  def update
    respond_to do |format|
      if @shift_good.update(shift_good_params)
        format.html { redirect_to @shift_good, notice: 'Shift good was successfully updated.' }
        format.json { render :show, status: :ok, location: @shift_good }
      else
        format.html { render :edit }
        format.json { render json: @shift_good.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /shift_goods/1
  # DELETE /shift_goods/1.json
  def destroy
    @shift_good.destroy
    respond_to do |format|
      format.html { redirect_to shift_goods_url, notice: 'Shift good was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_shift_good
      @shift_good = ShiftGood.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def shift_good_params
      params.require(:shift_good).permit(:good_id, :shift_id, :ves, :description)
    end
end
