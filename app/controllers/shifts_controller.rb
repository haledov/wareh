class ShiftsController < ApplicationController
  before_action :set_shift, only: [:show, :edit, :update, :destroy]


  # GET /shifts
  # GET /shifts.json
  def index
    @shifts = Shift.all
  end

  # GET /shifts/1
  # GET /shifts/1.json
  def show
    @goods = Good.all
    @grades = Grade.all
  end

  def goods
    @goods = Good.all
    
  end

  # GET /shifts/new
  def new
    @shift = Shift.new
    @goods = Good.all
    @grades = Grade.all
      Good.all.each do |good|

      if !@shift.shift_goods.exists?(good_id: good.id)
        @shift.shift_goods.build(good: good)
      end
    end
  end

  # GET /shifts/1/edit
  def edit
    @goods = Good.all
    @grades = Grade.all
    Good.all.each do |good|

      if !@shift.shift_goods.exists?(good_id: good.id)
        @shift.shift_goods.build(good: good)
      end
    end
  end

  # POST /shifts
  # POST /shifts.json
  def create
    @shift = Shift.new(shift_params)

    respond_to do |format|
      if @shift.save
        format.html { redirect_to edit_shift_path(@shift), notice: 'Shift was successfully created.' }
        format.json { render :edit, status: :created, location: edit_shift_path(@shift) }
      else
        format.html { render :new }
        format.json { render json: @shift.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /shifts/1
  # PATCH/PUT /shifts/1.json
  def update
    respond_to do |format|
      if @shift.update(shift_params)
        format.html { redirect_to shifts_path, notice: 'Shift was successfully updated.' }
        format.json { render :index, status: :ok, location: shifts_path }
      else
        format.html { render :edit }
        format.json { render json: @shift.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /shifts/1
  # DELETE /shifts/1.json
  def destroy
    @shift.destroy
    respond_to do |format|
      format.html { redirect_to shifts_url, notice: 'Shift was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_shift
      @shift = Shift.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def shift_params
      params.require(:shift).permit(:name, :description, :date, :shipper_id, :client_id, shift_goods_attributes: [:id, :ves, :good_id, :description, :_destroy])
    end
end
