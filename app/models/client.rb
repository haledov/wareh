class Client < ActiveRecord::Base
	acts_as_paranoid
	has_many :suplier_shifts, :class_name=>"Shift", :foreign_key => 'suplier_id'
	has_many :clients_shifts, :class_name=>"Shift", :foreign_key => 'client_id'
	has_many :shift_goods, through: :shifts, :foreign_key => 'client_id'
	has_many :shifts
end
