class Good < ActiveRecord::Base
  acts_as_paranoid
  belongs_to :grade
  has_many :shifts, through: :shift_goods
  has_many :shift_goods
  accepts_nested_attributes_for :shifts
  accepts_nested_attributes_for :shift_goods
end
