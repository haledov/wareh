class Shift < ActiveRecord::Base
  acts_as_paranoid
  belongs_to :suplier, :class_name => 'Client', :foreign_key => 'shipper_id'
  belongs_to :client, :class_name => 'Client', :foreign_key => 'client_id'
  has_many :shift_goods, :dependent => :delete_all
  has_many :goods, through: :shift_goods
  accepts_nested_attributes_for :goods
  accepts_nested_attributes_for :shift_goods, :allow_destroy => true, :reject_if => :all_blank


 
end
