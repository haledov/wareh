class ShiftGood < ActiveRecord::Base
	acts_as_paranoid
  belongs_to :good
  belongs_to :shift
  accepts_nested_attributes_for :shift, :reject_if     => :all_blank
  accepts_nested_attributes_for :good, :reject_if     => :all_blank
end
