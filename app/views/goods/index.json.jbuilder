json.array!(@goods) do |good|
  json.extract! good, :id, :grade_id, :name
  json.url good_url(good, format: :json)
end
