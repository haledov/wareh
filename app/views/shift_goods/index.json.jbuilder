json.array!(@shift_goods) do |shift_good|
  json.extract! shift_good, :id, :good_id, :shift_id, :ves, :description
  json.url shift_good_url(shift_good, format: :json)
end
