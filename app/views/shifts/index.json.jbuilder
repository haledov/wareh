json.array!(@shifts) do |shift|
  json.extract! shift, :id, :name, :description, :date, :shipper_id, :client_id
  json.url shift_url(shift, format: :json)
end
