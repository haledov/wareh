class CreateGoods < ActiveRecord::Migration
  def change
    create_table :goods do |t|
      t.references :grade, index: true
      t.string :name

      t.timestamps null: false
    end
    add_foreign_key :goods, :grades
  end
end
