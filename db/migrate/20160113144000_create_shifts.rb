class CreateShifts < ActiveRecord::Migration
  def change
    create_table :shifts do |t|
      t.string :name
      t.text :description
      t.date :date
      t.references :shipper, index: true
      t.references :client, index: true

      t.timestamps null: false
    end
    add_foreign_key :shifts, :clients, column: :shipper_id
    add_foreign_key :shifts, :clients, column: :client_id
  end
end
