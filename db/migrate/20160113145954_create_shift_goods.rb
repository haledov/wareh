class CreateShiftGoods < ActiveRecord::Migration
  def change
    create_table :shift_goods do |t|
      t.references :good, index: true
      t.references :shift, index: true
      t.decimal :ves, :precision => 10, :scale => 2
      t.text :description

      t.timestamps null: false
    end
    add_foreign_key :shift_goods, :goods
    add_foreign_key :shift_goods, :shifts
  end
end
