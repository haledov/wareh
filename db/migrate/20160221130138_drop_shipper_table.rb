class DropShipperTable < ActiveRecord::Migration
  def up
    remove_foreign_key('shifts', 'shipper')
    remove_foreign_key('shifts', 'client')
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
