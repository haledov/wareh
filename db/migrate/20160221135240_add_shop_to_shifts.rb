class AddShopToShifts < ActiveRecord::Migration
  def change
    add_column :shifts, :shop_id, :integer
    add_column :shifts, :suplier_id, :integer
    add_foreign_key :shifts, :shops
  end
end
