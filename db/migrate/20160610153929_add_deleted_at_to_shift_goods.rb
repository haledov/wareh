class AddDeletedAtToShiftGoods < ActiveRecord::Migration
  def change
    add_column :shift_goods, :deleted_at, :datetime
  end
end
