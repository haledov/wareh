class AddDeletedAtToGoods < ActiveRecord::Migration
  def change
    add_column :goods, :deleted_at, :datetime
  end
end
