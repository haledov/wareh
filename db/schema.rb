# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160610154028) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "clients", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.datetime "deleted_at"
  end

  create_table "goods", force: :cascade do |t|
    t.integer  "grade_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
  end

  add_index "goods", ["grade_id"], name: "index_goods_on_grade_id", using: :btree

  create_table "grades", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "shift_goods", force: :cascade do |t|
    t.integer  "good_id"
    t.integer  "shift_id"
    t.decimal  "ves",         precision: 10, scale: 2
    t.text     "description"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.datetime "deleted_at"
  end

  add_index "shift_goods", ["good_id"], name: "index_shift_goods_on_good_id", using: :btree
  add_index "shift_goods", ["shift_id"], name: "index_shift_goods_on_shift_id", using: :btree

  create_table "shifts", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.date     "date"
    t.integer  "shipper_id"
    t.integer  "client_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "shop_id"
    t.integer  "suplier_id"
    t.datetime "deleted_at"
  end

  add_index "shifts", ["client_id"], name: "index_shifts_on_client_id", using: :btree
  add_index "shifts", ["shipper_id"], name: "index_shifts_on_shipper_id", using: :btree

  create_table "shippers", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "shops", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "goods", "grades"
  add_foreign_key "shift_goods", "goods"
  add_foreign_key "shift_goods", "shifts"
  add_foreign_key "shifts", "shops"
end
