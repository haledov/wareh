require 'test_helper'

class ShiftGoodsControllerTest < ActionController::TestCase
  setup do
    @shift_good = shift_goods(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:shift_goods)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create shift_good" do
    assert_difference('ShiftGood.count') do
      post :create, shift_good: { description: @shift_good.description, good_id: @shift_good.good_id, shift_id: @shift_good.shift_id, ves: @shift_good.ves }
    end

    assert_redirected_to shift_good_path(assigns(:shift_good))
  end

  test "should show shift_good" do
    get :show, id: @shift_good
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @shift_good
    assert_response :success
  end

  test "should update shift_good" do
    patch :update, id: @shift_good, shift_good: { description: @shift_good.description, good_id: @shift_good.good_id, shift_id: @shift_good.shift_id, ves: @shift_good.ves }
    assert_redirected_to shift_good_path(assigns(:shift_good))
  end

  test "should destroy shift_good" do
    assert_difference('ShiftGood.count', -1) do
      delete :destroy, id: @shift_good
    end

    assert_redirected_to shift_goods_path
  end
end
